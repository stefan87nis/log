<?php



class User {
    
    private $db;
    public $id;
    public $name;
    public $email;
    public $password;
    public $new_password;
    public $repeat_password;
    public $created_at;
    public $deleted_at;
    
    
    function __construct($id = null) {
        $this->db  = require 'db/db.php';
        require_once 'Helper.class.php';
        //$conf = require 'db/config.php';
        
        
        
        if( $id ){
            $this->id = $id;
            $this->loadDb();
        }
    }
    
    public function loadDb(){
        $stmt_load = $this->db->prepare("
            SELECT * FROM `users`
            WHERE `id` = :id
            ");
        $stmt_load->execute([
            ':id' => $this->id
            ]);
        
        $user = $stmt_load->fetch();
        if( $user ){
            $this->name = $user->name;
            $this->email = $user->email;
            $this->password = $user->password;
            $this->acc_type = $user->acc_type;
            $this->created_at = $user->created_at;
            $this->deleted_at = $user->deleted_at;
        }
    }
    
    
    public function isNameValid(){
        
        if($this->name == ""){
            Helper::addError("Name is empty");
            return false;
        }
        return true;
    }
    
    public function isPasswordValid(){
        
        if($this->new_password  == ""){
            Helper::addError("Password is empty");
            return false;
        }
        
        if($this->new_password  != $this->repeat_password){
            Helper::addError("Password not match");
            return false;
        }   
        
        if(strlen($this->new_password)  < 6){
            Helper::addError("Password is short");
            return false;
        }
        
        return true;
    }
    
    
    public function isEmailValid(){
        
        if($this->email  == ""){
            Helper::addError("Email is empty");
            return false;
        }
        
        if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)){
            Helper::addError("You need to use real email address");
        }
        
        return true;
        //validacija za jedinstveni email u slucaju da vec postoji 
    }
    
    public function insert(){
        
        if(!$this->isNameValid()){
        return false;
        }
        
        if(!$this->isPasswordValid()){
            return false;
        }
        
        if(!$this->isEmailValid()){
            return false;
        }
        
        $this->password = md5($this->new_password);
        
        $stmt_insert = $this->db->prepare("
            INSERT INTO `users`
            (`name`,`email`,`password`)
            VALUES
            (:name, :email ,:password)
            ");
    $result = $stmt_insert->execute([
            ':name' => $this->name,
            ':email' => $this->email,
            ':password' => $this->password
            ]);
        
        if($result){
            $this->id = $this->db->lastInsertId();
            $this->loadDb();
        }
        return $result;
    }
    
    
    public function update(){
        
        
        if( $this->new_password && $this->passwordIsValid() ) {
        $this->password = md5($this->new_password);
        }

        $stmt_update = $this->db->prepare("
            UPDATE `users`
            SET 
            `name` = :name,
            `email` = :email,
            `password` = :password,
            `acc_type` = :acc_type
            WHERE `id` = :id
            ");
    return  $stmt_update->execute([
            ':name' => $this->name,
            ':email' => $this->email,
            ':password' => $this->password,
            ':acc_type' => $this->acc_type,
            ':id' => $this->id
            ]);
    }
    
    public function delete(){
        $stmt_delete = $this->db->prepare("
            UPDATE `users`
            SET `deleted_at` is now()
            WHERE `id` = :id
            ");
    return  $stmt_delete->execute([
            ':id' => $this->id
            ]);
    }
    
    public function all(){
        $stmt_all = $this->db->prepare("
            SELECT * FROM `users`
            ");
        $stmt_all->execute();
        return $stmt_all->fetchAll();
    }
    
    //LOGIN
    public function login(){
        
        $stmt_login = $this->db->prepare("
            SELECT * FROM `users`
            WHERE `email` = :email
            AND `password` = :password
            ");
        $stmt_login->execute([
            
            ':email' => $this->email,
            ':password' => md5($this->password) 
            ]);
        
        $user = $stmt_login->fetch();
        
        
        if($user){
            Helper::addMessage('Login successfull!');
            Helper::sessionStart();
            $_SESSION['user_id'] = $user->id;
            return true;
        }
        Helper::addError("INVALID CREDENCIALS");
        return false;
        
        }
        
        public static function isUserLoggedIn(){
            require_once 'Helper.class.php';
            Helper::sessionStart();
            
            return isset($_SESSION['user_id']);
        }
                
        public static function getUserId(){
            require_once 'Helper.class.php';
            Helper::sessionStart();
            
            if(isset($_SESSION['user_id'])){
                return $_SESSION['user_id'];
            }else{
                return false;
            }
        }   
        
        public function loadLoggedInUser(){
            require_once 'Helper.class.php';
            Helper::sessionStart();
            
            if(isset($_SESSION['user_id'])){
                $this->id = $_SESSION['user_id'];
                $this->loadDb();
            }
        }
        
        public function search($query){
            $search = "%$query%";
            $stmt_search = $this->db->prepare("
                SELECT *
                FROM `users`
                WHERE (
                `name` LIKE :query
                 OR `email` LIKE :query
                 ) 
                 AND `deleted_at` IS NULL
                ");
            $stmt_search->execute([ ':query' => $search  ]);
            return $stmt_search->fetchAll();
        }
        
        
        public function paginate($page = 1){
            $conf = require 'db/config.php';
            $offset = ($page - 1) * $conf['number_of_users'];
            $stmt_paginate = $this->db->prepare("
                SELECT * FROM `users`
                WHERE `deleted_at` IS NULL
                LIMIT {$conf['number_of_users']};
                OFFSET{$offset}    
                ");
            $stmt_paginate->execute();
            return $stmt_paginate->fetchAll();
        }
        
        public function numOfUsers(){
            $stmt_all_users = $this->db->prepare("
                SELECT count(*) as number_of_users
                FROM `users` 
                WHERE `deleted_at` IS  NULL
                ");
            $stmt_all_users->execute();
            return $stmt_all_users->fetch()->number_of_users;
        }
    }