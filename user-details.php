<?php 
    require_once 'class/User.class.php';

    $user = new User();
    $user->loadLoggedInUser();
    
    include_once  'inc/header.inc.php';
?>



<div class="my-5">
    <div class="row">

        <div class="col-md-5">
            <img src="" class="product-details-img" />
        </div>

      <div class="col-md-7">
          <h5 class="mt-5">Email : <?php echo $user->email; ?></h5>
            </p>

        <div class="d-flex flex-column align-items-end">
        <h5 class="mt-5">Account type: <?php echo $user->acc_type; ?></h5>
        <h5 class="my-5">Name: <?php echo $user->name; ?></h5>
        </div>
      </div>
    </div>
    
    
    <div class="d-flex flex-column align-items-end"> 
        <a href="./table.php" class="btn btn-info">BACK</a> 
   </div>  
</div>

<?php include_once 'inc/footer.inc.php'; ?>
