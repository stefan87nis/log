<?php

require_once 'class/User.class.php';

$u = new User();

if( isset($_GET['page']) ) {
  $page = $_GET['page'];
} else {
  $page = 1;
}



if( isset($_GET['search']) ) {
  $users = $u->search($_GET['search']);
} else {
  $users = $u->paginate($page);
}

$totalNumOfProducts = $u->numOfUsers();
$conf = require 'db/config.php';
$numOfPages = ceil($totalNumOfProducts / $conf['number_of_users']);


if( $page <= 1 ) {
  $prev = 1;
  // $prev = $numOfPages;
} else {
  $prev = $page - 1;
}

if( $page >= $numOfPages ) {
  $next = $numOfPages;
  // $next = 1;
} else {
  $next = $page + 1;
}

?>

<?php include 'inc/header.inc.php'; ?>

<?php if( isset($_GET['search']) && $_GET['search'] != "" ) { ?>
<h1 class="my-5">Search results for "<?php echo $_GET['search'] ?>"</h1>
<?php } else { ?>
<h1 class="my-5">Products</h1>
<?php } ?>


<div class="row">
    <?php foreach ($users as $user) { ?>
    <div class="col-md-4">
        <div class="mb-4">
        <div class="card" style="width: 18rem;">
          <img src="..." class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title"><?php echo $user->name; ?></h5>
            <p class="card-text"><?php echo $user->email; ?></p>
            <p class="card-text"><?php echo $user->acc_type; ?></p>
            <a href="user-details.php?id=<?php  echo $user->id;  ?>"  class="btn btn-primary">DETAILS</a>
          </div>
       </div>
       </div>     
    </div>  
    <?php } ?>    
</div>


<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-center">
    <li class="page-item">
      <a class="page-link" href="./table.php?page=<?php echo $prev; ?>" aria-disabled="true">Previous</a>
    </li>

    <?php for($i = 1; $i <= $numOfPages; $i++) { ?>
      <li class="page-item<?php
          if( $page == $i ) {
            echo " active";
          }
        ?>">
        <a
          class="page-link"
          href="./table.php?page=<?php echo $i; ?>">
          <?php echo $i; ?>
        </a>
      </li>
    <?php } ?>
    <li class="page-item">
      <a class="page-link" href="./table.php?page=<?php echo $next; ?>">Next</a>
    </li>
  </ul>
</nav>

<?php include 'inc/footer.inc.php'; ?>
