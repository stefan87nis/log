<?php 
    require_once 'class/User.class.php';
    require_once 'class/Helper.class.php';

    if(isset($_POST['btn_login'])){
        $loginUser = new User();
        $loginUser->email = $_POST['email'];
        $loginUser->password = $_POST['password'];
        if($loginUser->login()){
            header("Location: ./index.php");
            die();
        }
    }


    include_once 'inc/header.inc.php';
?>
          

<h1 class="my-5" align="center">LOGIN PAGE</h1>
              
    
    
    
    <form action="" method="post">
        <div class="form-group col-md-6">
          <label for="inputEmail">Email address</label>
          <input type="email" name="email" class="form-control" id="inputEmail"  placeholder="Enter email">
        </div>
        <div class="form-group col-md-6">
          <label for="inputPassword">Password</label>
          <input type="password" name="password" class="form-control" id="inputPassword" placeholder="Password">
        </div>
        <div class="form-group col-md-6">
        <button name="btn_login" type="submit" class="btn btn-primary">Login</button>
        </div>
    </form>
    
    
    
    
    
    
    
<?php include_once 'inc/footer.inc.php';  ?>