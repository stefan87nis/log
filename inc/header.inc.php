<?php
    require_once 'class/Helper.class.php';
    require_once 'class/User.class.php';
    
    $showError = Helper::getError();
    $showMessage = Helper::getMessage();

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>Hello, world!</title>
  </head>
  <body>
      <!-- navbar -->
      
<?php include_once 'navbar.inc.php'; ?>
      
      <!-- navbar -->
      
      
      
      <div class="container">
          
          
          <?php if($showError) {  ?>
          <b>Error!</b>
          <div class="alert alert-danger my-4 col-md-3">
          <?php echo $showError; ?>
          </div>
          <?php  } ?>

          
          <?php if($showMessage) {  ?>
          <div class="alert alert-success my-4">
          <b>Success!</b>    
          <?php echo $showMessage; ?>
          </div>
          <?php  } ?>