<?php 
    require_once 'class/User.class.php';
    require_once 'class/Helper.class.php';
    
    
    
    
    if(isset($_POST['register_btn'])){
        $newUser = new User();
        $newUser->name = $_POST['name'];
        $newUser->email = $_POST['email'];
        $newUser->new_password = $_POST['password'];
        $newUser->repeat_password = $_POST['repeat_password'];
        if($newUser->insert()){
            Helper::addMessage("YOU ARE REGISTERED");
        }
    }

include_once 'inc/header.inc.php';
?>

<h1 class="my-5" align="center">REGISTRATION PAGE</h1>
              
    
<form action="" method="post" class="col-md-6">
        <div class="form-group">
          <label for="inputName">Name</label>
          <input type="text" name="name" class="form-control" id="inputName"  placeholder="Enter name">
        </div>
        <div class="form-group">
          <label for="inputEmail">Email address</label>
          <input type="email" name="email" class="form-control" id="inputEmail"  placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="inputPassword">Password</label>
          <input type="password" name="password" class="form-control" id="inputPassword"  placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="inputPasswordAgain">Password</label>
          <input type="password" name="repeat_password" class="form-control" id="inputPasswordAgain" placeholder="Password">
        </div>
        <button name="register_btn"  class="btn btn-primary">Register</button>
    </form>
    
    
    
<?php include_once 'inc/footer.inc.php';  ?>