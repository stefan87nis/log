<?php

$db = require './db.php';


$stmt_createUsersTable = $db->prepare("
  CREATE TABLE IF NOT EXISTS `users` (
    `id` int AUTO_INCREMENT PRIMARY KEY,
    `name` varchar(30),
    `email` varchar(30),
    `password` varchar(32),
    `acc_type` enum('user', 'admin')DEFAULT 'user',
    `created_at` datetime DEFAULT now(),
    `deleted_at` datetime DEFAULT NULL
  )
");
$stmt_createUsersTable->execute();


$stmt_selectUser = $db->prepare("
    SELECT * FROM `users`
    ");
$stmt_selectUser->execute();

$stmt_uniqueAdmin = $stmt_selectUser->rowCount();

if( $stmt_uniqueAdmin <= 0){
    $stmt_uniqueAdmin = $db->prepare("
     INSERT INTO `users`
     (`name`,`email`,`password`,`acc_type`)     
     VALUES
     (:name,:email,:password,:acc_type)
    ");
    $stmt_uniqueAdmin->execute([
        ':name' => "admin",
        ':email' => "admin@shop.com",
        ':password' => md5("123"),
        ':acc_type' => "admin"
        ]);
}